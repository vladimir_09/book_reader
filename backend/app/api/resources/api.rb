# frozen_string_literal: true

require 'rack/cors'

module Resources
  class API < Grape::API
    mount Resources::Users
    mount Resources::Documents
  end
end
