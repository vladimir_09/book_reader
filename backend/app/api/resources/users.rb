# frozen_string_literal: true

module Resources
  # users authentication
  class Users < Grape::API
    namespace 'users' do
      post 'login' do
        {params: [params[:login], params[:password]]}
      end

      get 'login' do
        {}
      end

      post 'register' do
        { params: [params[:login],
        params[:email],
        params[:password],
        params[:confirmation]]
      }
      end

      get 'register' do
        {}
      end
    end
  end
end
