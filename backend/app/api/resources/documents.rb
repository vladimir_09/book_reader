# frozen_string_literal: true

module Resources
  # read and upload documents
  class Documents < Grape::API
    namespace 'documents' do
      post 'upload_doc' do
        data = params[:document][:tempfile].read
        data = 'huge document' if data.force_encoding('ISO-8859-1').encode('UTF-8').size > 1.megabyte
        {'test' => params, file: data.split }
      end
    end
  end
end
