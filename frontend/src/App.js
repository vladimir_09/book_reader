import { Link } from "react-router-dom";

function App() {
  return (
    <div className="App">
      <h1>Welcome to my react app!</h1>
        <p>
          Главная страница читалки в 1 символ, пока тут только переход на загрузку файла
        </p>
      <br />
       <p>
          <Link to="/upload">Upload Page</Link>
       </p>
       <p>
          <Link to="/read">Reader page</Link>
       </p>
       <p>
          <Link to="/register">Register</Link>
       </p>
       <p>
          <Link to="/login">Login</Link>
       </p>
    </div>
  );
}

export default App;
