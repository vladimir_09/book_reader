import React from 'react';
import axios from 'axios';
import { Link } from "react-router-dom";

class Login extends React.Component {
    state = {
        login: '',
        password: ''
    }

    handleLogin = (event) => {
        const {target: {value}} = event;

        this.setState({login:value});
    }

    handlePassword = (event) => {
        const {target: {value}} = event;

        this.setState({password:value});
    }

    handleSubmit = (state) => {
        
        const {login, password} = state;

        axios.post("http://localhost:3000/api/users/login", {
            login: login,
            password: password
        }).then((response) => {});
    }

    render() {
        const {login, password} = this.state;

        return (
            <div id="login">
            <h2>Login:</h2>
                <form onSubmit={this.handleSubmit}>
                    <p>Name: 
                        <input type="text" id="username" name="username" value={login} onChange={this.handleLogin} />
                    </p>
                    <p>Password:
                        <input type="password" id="password" name="password" value={password} onChange={this.handlePassword} />
                    </p>
                    <input type="submit" onClick={this.handleSubmit}/>
                </form>
                <Link to="/">Home</Link>
            </div>
            
        );
    }
}

export default Login