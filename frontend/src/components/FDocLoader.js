import axios from "axios";
import React, {Component} from "react";
import { Link } from "react-router-dom";

class DocumentLoader extends Component {
  state = {
    selectedFile: null
  };

  onFileChange = event => {
    this.setState({selectedFile: event.target.files[0]});
  };

  onFileUpload = () => {
    const formData = new FormData();
    formData.append(
      "document",
      this.state.selectedFile,
      this.state.selectedFile.name
    );
    console.log(formData);
    axios.post("http://localhost:3000/api/documents/upload_doc", formData);
  };

  fileData = () => {
    if (this.state.selectedFile) {
      return (
        <div>
          <h2>File Details:</h2>
          <p>File Name: {this.state.selectedFile.name}</p>

          <p>File Type: {this.state.selectedFile.type}</p>

        </div>
      );
    } else {
      return (
        <div>
          <br />
          <h4>Выберите файл прежде чем нажать кнопку загрузить</h4>
        </div>
      );
    }
  };

  render() {
    
    return (
      <div>
          <h1>
            Ruby Reader
          </h1>
          <h3>
            Загрузить файл для чтения
          </h3>
          <div>
              <input type="file" onChange={this.onFileChange} />
              <button onClick={this.onFileUpload}>
                Загрузить
              </button>
          </div>
          <Link to="/">Main page</Link>
        {this.fileData()}
      </div>
    );
  }
}

export default DocumentLoader;