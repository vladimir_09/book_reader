import React from 'react'
import { Link } from "react-router-dom";
import axios from 'axios';

class Register extends React.Component {
    state = {
        login: '',
        email: '',
        password: '',
        confirmation: ''
    }

    handleLogin = event => {
        this.setState({ login: event.target.value});
    }

    handleEmail = event => {
        this.setState({ email: event.target.value});
    }

    handlePassword = event => {
        this.setState({ password: event.target.value});
    }

    handleConfirmation = event => {
        this.setState({ confirmation: event.target.value});
    }     
     
    handleSubmit = state => {
        // event.preventDefault();
        axios.post("http://localhost:3000/api/users/register", {
            username: this.state.login,
            email: this.state.email,
            password: this.state.password,
            confirmation: this.state.confirmation
        }).then(res => {
            if (res.data === true) {
                window.location.href = "http://localhost:80/login"
            } else {
                alert("There is already a user with this email")
            }
        }).catch(() => {
            alert("An error occurred on the server")
        })
    }

    render () {
        return (
            <div className="register-form">
                <h2>Register user:</h2>
                <form onSubmit={this.handleSubmit}>
                    <p>Name: 
                        <input type="text" id="username" name="username" value={this.login} onChange={this.handleLogin} />
                    </p>
                    <p>Email: 
                        <input type="email" id="email" name="email" value={this.email} onChange={this.handleEmail} />
                    </p>
                    <p>Password:
                        <input type="password" id="password" name="password" value={this.password} onChange={this.handlePassword} />
                    </p>
                    <p>Password confirmation:
                        <input type="password" id="confirmation" name="confirmation" value={this.confirmation} onChange={this.handleConfirmation} />
                    </p>
                    <input type="submit" onClick={this.handleSubmit}/>
                </form>
                <Link to="/">Home</Link>
            </div>
        );
    }
}

export default Register