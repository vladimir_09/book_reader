import React from 'react';
import axios from 'axios';
import { Link } from "react-router-dom";

class Reader extends React.Component {

    state = {
        chapter: [],
        speed: 0,
        count: 0,
        chapterNum: 0,
        book: ''
    }

    handleSpeed = (event) => {
        this.setState({speed:event.value});
    }

    handleBook = (event) => {
        this.setState({book:event.value});
    }

    handleWords = state => {
        axios.post("https://localhost:3000/api/get_words", {
            book: this.book,
            page: this.chapterNum
        }).then((response) => {
            this.setState({chapter:response.data});
            this.setState({chapterNum:this.chapterNum+1})
        })
    }
    
    render () {
        return (
            <div class="reader">
                <h1>Ридер</h1>
                <form onSubmit={this.handleWords}>
                    <p>Введите название книги
                        <input type="book" id="book" name="book" value={this.book} onChange={this.handleBook} />
                    </p>
                    <p>Введите скорость вывода текста (слов в секунду)
                        <input type="speed" id="speed" name="speed" value={this.speed} onChange={this.handleSpeed} />
                    </p>
                    <button type="submit" onSubmit={this.handleWords}>Submit</button>
                    <p><Link to="/">Main page</Link></p>
                    <p><Link to="/upload">Upload page</Link></p>
                </form>
            </div>
        )
    }
}

export default Reader